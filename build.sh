#!/bin/sh

set -euxo pipefail
SELF=$(realpath "$0")
SELFDIR=$(dirname "$SELF")
RESULT_DIR="$SELFDIR/results"
RELEASE=41

_build_srpm() {
  SPEC=$(basename "$1")
  DIR=$(dirname "$1")
  pushd "$DIR"
  mock -r fedora-$RELEASE-x86_64 --buildsrpm --spec "$SPEC" --sources $PWD --resultdir "$PWD"
  popd
}

_build_rpm() {
  mock -r fedora-$RELEASE-x86_64 --localrepo="$RESULT_DIR" --continue --chain $*
}

_build_all_srpm() {
  find . -name "*.spec" | while read spec; do
    _build_srpm "$spec"
  done
}

_build_all_rpm() {
  # Order matters...
  _build_rpm ./rust-range-map-vec/rust-range_map_vec-*.src.rpm \
             ./rust-bitfield-struct/rust-bitfield-struct-*.src.rpm \
             ./rust-open-enum-derive/rust-open-enum-derive-*.src.rpm \
             ./rust-open-enum/rust-open-enum-*.src.rpm \
             ./rust-igvm/rust-igvm_defs-*.src.rpm \
             ./rust-igvm/igvm-*.src.rpm
}

_buld_all() {
  _build_all_srpm
  _build_all_rpm
}

case $1 in
  srpm)
    _build_srpm $2
    ;;
  rpm)
    _build_rpm $2
    ;;
  all_srpm)
    _build_all_srpm
    ;;
  all_rpm)
    _build_all_rpm
    ;;
  all)
    _build_all
    ;;
  *)
    echo "unknown command $1"
    exit 1
    ;;
esac

