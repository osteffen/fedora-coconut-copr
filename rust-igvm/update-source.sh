#!/bin/bash -eux
D=${PWD}
DIR=$(mktemp -d)
VERSION=0.3.3
trap "rm -rf \${DIR}" exit
TARGET_DIR=$(realpath "${PWD}")
pushd "${DIR}"

#git clone https://github.com/osteffenrh/igvm.git
git clone https://github.com/microsoft/igvm.git
mv igvm igvm-$VERSION

### Not needed for v0.3.2 - do downstream patches needed!
# igvm-defs crate
# pushd igvm-$VERSION
# git checkout fedora
# pushd igvm_defs
# cargo package
# popd
# cp target/package/igvm_defs*.crate "$TARGET_DIR"
# git clean -xdf
# 
# # Repo + patches
# git format-patch -o "$TARGET_DIR" igvm-v$VERSION
# git checkout igvm-v$VERSION
# popd # igvm
tar -c --exclude-vcs --exclude-vcs-ignores -v igvm-$VERSION | xz -c -9 -T0 > igvm.tar.xz
mv igvm.tar.xz "$TARGET_DIR"

popd # DIR
