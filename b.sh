#!/bin/sh -xe
SELF=$(realpath "$0")
SELFDIR=$(dirname "$SELF")
NAME=${1:-$(dirname "$(realpath .)")}
fedpkg --release=f40 --name $NAME srpm
SRPM=$(find . -iname "*fc40\.src\.rpm" | sort --version-sort | head -n1)
mock -r fedora-40-x86_64 --localrepo="$SELFDIR/results" --chain "$SRPM"
