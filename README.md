# Coconut SVSM for Fedora

This is a collection of spec files to build rpm packages for some things needed
to install and run confidential virtual machines under AMD SEV-SNP, using the
Coconut-SVSM.

It currently includes:
- The IGVM Rust library, wrapped as a static C library
  - and all Rust dependencies currently not available in Fedora.

# ToDo
- Include IGVM Rust crates (currently disabled due to difficulties around the
  cargo-workspace feature used by the upstream IGVM project).
- Check that all Licenses are correct / License files included
- Check that all documentation is included
- Package names of the igvm packages
- Upstream patches

